//
//  Example.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 06..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

func example(of name: String, action: () -> Void) {
    print("\n----- Example of \"\(name)\" ----")
    action()
}
