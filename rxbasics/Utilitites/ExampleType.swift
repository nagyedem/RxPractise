//
//  ExampleType.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 07..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import RxSwift

protocol ExampleType {
    var title: String { get }
    var disposable: Disposable { get }
    var disposeHandler: DisposeHandler { get }
    var color: UIColor { get }
}
