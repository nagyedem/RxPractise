//
//  ViewController.swift
//  rxbasics
//
//  Created by Nagy Ádám on 2018. 08. 06..
//  Copyright © 2018. Nagy Ádám. All rights reserved.
//

import UIKit

// MARK: - Properties
class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    lazy var dataHandler = DataHandler()
}

// MARK: - LifeCycle
extension ViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "RxSwift Basics"
    }
}

// MARK: - UITableView Delegates
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataHandler.datas.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataHandler.datas[section].practises.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dataHandler.datas[section].title
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rid", for: indexPath)
        let practise = dataHandler.datas[indexPath.section].practises[indexPath.row]
        cell.textLabel?.text = practise.title
        cell.backgroundColor = practise.color
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        let practise = dataHandler.datas[indexPath.section].practises[indexPath.row]
        dataHandler.call(test: practise)
    }
}

